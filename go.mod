module github.tattsgroup.com/TattsGroup/processor

go 1.13

require (
	github.com/eoscanada/eos-go v0.9.0
	github.com/golang/protobuf v1.3.3
	github.com/spf13/viper v1.6.2
	github.com/tidwall/gjson v1.6.0
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	google.golang.org/grpc v1.28.0
)
